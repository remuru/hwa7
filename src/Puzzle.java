
//Basis for solution: https://gist.github.com/AhmadElsagheer/d96d1081af545834e32f3624b06c762a - slightly modified
public class Puzzle {
   static boolean[] usedLetter; //if letter with such index is used, the value is true
   static boolean[] usedDigit; //if digit is used, value is true
   static int[] assignedDigit; //digit assigned to the char index
   static int solutions = 0;
   /** Main entry point for program.
    */
   static void main (String[] args){
      String w1 = args[0];
      String w2 = args[1];
      String w3 = args[2];

      solve(w1, w2, w3);
      }
   /** Coordinate solving, finish with printout of total solutions.
    */
      static void solve(String w1, String w2, String w3)
      {
         solutions = 0;
         usedLetter = new boolean[26];	// usedLetter[i] = true if letter i appears in w1, w2 or w3
         usedDigit = new boolean[26];	// usedDigit[i] = true if digit i is used by a letter (used in backtracking)
         assignedDigit = new int[26];    // assignedDigit[i] = digit assigned to letter i (used in backtracking)
         markLetters(w1); markLetters(w2); markLetters(w3);
         System.out.println("Solutions for puzzle: " + w1 + " + " + w2 + " = " + w3);
         backtrack(0, w1, w2, w3);
         System.out.println("Found total of " + solutions + " solutions.");
         System.out.println("------------");
      }

   /** Mark ascii letter indexes that are used by the words.
    * @param w input string
    */
      static void markLetters(String w)
      {
         for(int i = 0; i < w.length(); ++i)
            usedLetter[w.charAt(i) - 'A'] = true;
      }
   /** Isthe solution valid
    *
    * @param w1, w2, w3 - input strings
    * @return true if solution is valid
    */
      static boolean check(String w1, String w2, String w3)
      {
         if(leadingZero(w1) || leadingZero(w2) || leadingZero(w3))
            return false;
         return value(w1) + value(w2) == value(w3);
      }
   /** Is 0 assigned to a letter that is placed at the beginning of a word
    * @return true if 0 assigned to a letter that is placed at the beginning of a word
    */
      static boolean leadingZero(String w) { return assignedDigit[w.charAt(0) - 'A'] == 0; }

   /** Calculate value of words
    * @param w - input word
    * @return calculated value according to assigned digits
    */
      // if w = ABCD, then the function returns A * 1000 + B * 100 + C * 10 + D.
      static long value(String w)
      {
         long val = 0;
         for(int i = 0; i < w.length(); ++i)
            val = val * 10 + assignedDigit[w.charAt(i) - 'A'];
         return val;
      }
   /** Iterate through solution combinations
    * @param char_idx - index character viewed
    */
      static void backtrack(int char_idx, String w1, String w2, String w3)
      {
         if(char_idx == 26)
         {
            // finished assigning values for the 26 letters
            if(check(w1, w2, w3))
            {
               solutions++;
               for(int i = 0; i < 26; ++i)
                  if(usedLetter[i])
                     System.out.printf("[%c = %d]", (char)(i + 'A'), assignedDigit[i]);
               System.out.println("(" + value(w1) + " + " + value(w2) + " = " + value(w3) + ")");
            }
            return;
         }

         if(!usedLetter[char_idx])
         {
            // skip this letter, it was not used in the input.
            backtrack(char_idx + 1, w1, w2, w3);
            return;
         }
         // try assigning different digits for this letter
         for(int digit = 0; digit < 10; ++digit)
            if(!usedDigit[digit])	// this condition guarantees that no digit is used for more than one letter
            {
               usedDigit[digit] = true;
               assignedDigit[char_idx] = digit;
               backtrack(char_idx + 1, w1, w2, w3);
               usedDigit[digit] = false;
            }
      }
   }

